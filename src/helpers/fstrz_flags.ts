// the flags should be ordered longest to shortest
const fstrz_flags = {
    'w,p': 'optimisation en cours',
    'w,c': 'réponse en cours d\'optimisation mais servie cachée',
    'Z,p': 'page non optimisable (ne sera jamais optimisée, 404, 301, 302, POST, etc ...)',
    'uc': 'User Canceled, la requête a été annulée par l\'utilisateur',
    'cbo': 'Circuit Breaker Open, l\'objet n\'est pas optimisé suite à l\'indisponibilité temporaire d\'un composant du moteur',
    'ccb': 'Cache CallBack, l\'objet est servi par le proxy depuis le cache (cas d\'un objet qui n\'a pas encore été inclus dans un test A/B)',
    'of': 'Overflow, indique que le système de débordement est en place',
    'tb': 'too big, la ressource est trop lourde pour être optimisée',
    'dc': 'cache dynamique (fonctionnement interne du moteur)',
    'sc': 'la page est cachée via le SmartCache (Cache + Ajax)',
    'clc': 'la page est cachée via le Cookie Less Cache',
    'pl': 'des liens de preload ont été insérés',
    'ed': 'moteur désactivé',
    'vf': 'objet virtuel reconstitué',
    '!c': 'non cachée',
    '!o': 'non optimisée',
    'p': 'requête/réponse proxifiée',
    'h': 'contenu qui n\'est pas du HTML (JSON envoyé avec un content type text/html par exemple)',
    'o': 'optimisée',
    'c': 'cachée',
    'v': 'objet virtuel (résultat d\'une concaténation ou d\'un outlining)',
    't': 'timeout, l\'optimisation a pris trop de temps, la page renvoyée n\'est pas optimisée',
    'e': 'error, l\'optimisation a échoué, la page renvoyée n\'est pas optimisée',
    'b': 'blocked, l\'adresse IP est bloquée'
};

export default fstrz_flags;